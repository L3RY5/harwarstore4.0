# Rest services 
# Find all products 
- Url : http://localhost:8080/hardwareFrontend/api/product/all/{product_naam}
- Response 
* products: ObjectArray
    *  id : int
    *  manufactory : String
    *  warranty : int
    *  releaseDate : date 
    *  name : String
    *  price : int 
    *  IsWireless  : String



- example


```
"Product" : [ 
{“id”: “int”,
 “manufatury”: “String”,
 “warranty”: “int”, 
“releaseDate” : “Date”,  
“name” : “String”, 
“price” : “int”, 
“IsWireless” : “String”}
...
]
```


# Find by id 
# Rest services 
- Url : http://localhost:8080/hardwareFrontend/api/Poduct/one/{product_id}/{product_naam}
- Response 
* products: ObjectArray
    *  id : int
    *  manufactory : String
    *  warranty : int
    *  releaseDate : date 
    *  name : String
    *  price : int 
    *  IsWireless  : String

- example


```
[{"id":9000,"manufactory":"manufactory2","name":"Mouse Master","price":10.0,"releaseDate":"2017-10-20","warranty":1,"wireless":"wireless"}]
```


# server installation

* First of all download WildFly15.0.1
    * Then go in to the folder and click on bin
    * there you will find the "add-user.bat" file
    * double click on it
    * Add a management user
    * enter a username and pasword and say yes to verything

* Then click on the Modules folder and go to the com folder
    * in this folder add a mysql/main folder
    * In the main folder add a module.xml folder that contains

    <?xml version="1.0"?>
<module name="com.mysql" xmlns="urn:jboss:module:1.5">
	<resources>
		<resource-root path="mysql-connector-java-8.0.12.jar"/>
	</resources>
<dependencies>
	<module name="javax.api"/>
	<module name="javax.transaction.api"/>
</dependencies>
</module>
* and add in the main folder this jar
    * mysql-connector-java-8.0.12.jar

* in persistance.xml  
    * <jta-data-source>java:/test</jta-data-source>
    * <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost:3306/hardwarestore"/>
