package productRestService;


import com.realdolmen.teamPHP.dtos.ProductDTO;
import com.realdolmen.teamPHP.facade.ProductFacade;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("product")
public class RestService {
    List<ProductDTO> productDTOS;
    @Inject
    private ProductFacade productFacade;


    @GET
    @Path("all/{product_naam}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllProducts(@PathParam("product_naam")String product){
        productDTOS = new ArrayList<>();
        productDTOS = (productFacade.findAll(product));
        return Response.status(Response.Status.OK)
                .entity(productDTOS)
                .build();
    }

    @GET
    @Path("one/{product_id}/{product_naam}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("product_id")String id,@PathParam("product_naam") String product){
        productDTOS = new ArrayList<>();
        productDTOS.add(productFacade.findProduct(product,Long.parseLong(id)));
            return Response.status(Response.Status.OK)
                    .entity(productDTOS)
                    .build();

    }

}

