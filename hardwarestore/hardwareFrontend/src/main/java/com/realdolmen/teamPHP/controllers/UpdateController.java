package com.realdolmen.teamPHP.controllers;

import com.realdolmen.teamPHP.dtos.ProductDTO;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class UpdateController implements Serializable {
    private Boolean showInput ;
    private List<String> productToUpdate;
    private ProductDTO dto;
    private String productype;

    public UpdateController(){

    }
    @PostConstruct
    public void init() {
        System.out.println("In init method van UpdateController");
        showInput = false;
        productToUpdate = new ArrayList<>();
        dto = null;
    }
    public Boolean currentState(){
        System.out.println("currently in currentState");
       return  showInput;
    }
    public void setCurrentState(){
        if(showInput){
            System.out.println("current state is true");
            showInput = false;
            //fillList(waarde);
            //return currentState();
        }
        else {
            showInput = true;
            //return  currentState();
        }
    }
    public boolean isShowInput() {
        return showInput;
    }
    public void setShowInput(boolean showInput) {
        this.showInput = showInput;
    }

    public void fillList(String waarde){
        if(showInput) {
            productToUpdate.add(waarde);
            System.out.println(waarde + " werd toegevoegd an de lijst");
        }
    }
}