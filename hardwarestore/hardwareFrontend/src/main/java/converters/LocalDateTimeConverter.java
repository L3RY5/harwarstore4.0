package converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@FacesConverter(value="localDateTimeConverter")
public class LocalDateTimeConverter implements javax.faces.convert.Converter {
    //method is used to parse a String from the component and return it as a java.time.LocalDate
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return LocalDate.parse(value);
    }

    //accepts a LocalDate object and returns it as a String in the specified date format
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        LocalDate dateValue = (LocalDate) value;
        return dateValue.format( DateTimeFormatter.BASIC_ISO_DATE);
        //return dateValue.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
