package com.realdolmen.teamPHP.dtos;

import com.realdolmen.teamPHP.domain.ShopBasket;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

public   class ProductDTO {
    private Long id;
    private Double price;
    private String name;
    private String manufactory;
    private LocalDate releaseDate;
    //wrm is warranty een string het is toch gebaseerd op tijd ?
    private Integer warranty;
    private ShopBasket shopbasket;
    private Long rpm;
    private String isWireless ;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufactory() {
        return manufactory;
    }

    public void setManufactory(String manufactory) {
        this.manufactory = manufactory;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getWarranty() {
        return warranty;
    }

    public void setWarranty(Integer warranty) {
        this.warranty = warranty;
    }

    public ShopBasket getShopbasket() {
        return shopbasket;
    }

    public void setShopbasket(ShopBasket shopbasket) {
        this.shopbasket = shopbasket;
    }

    public Long getRpm() {
        return rpm;
    }

    public void setRpm(Long rpm) {
        this.rpm = rpm;

    }
    public String getWireless() {
        return isWireless;
    }

    public void setWireless(Boolean wireless) {
        if(wireless){
            isWireless = "wireless";
        }
        else {
            isWireless = "not wireless";
        }
    }

}
