package com.realdolmen.teamPHP.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Product {

    // Id voor product ? ik ga nu een id zetten we zien morgen wel
    @Id
    @GeneratedValue
    private Long id;
    private Double price;
    private String name;
    private String manufactory;

    //@Temporal(TemporalType.DATE)
   // @Column(nullable = false)
    private LocalDate releaseDate;

    //wrm is warranty een string het is toch gebaseerd op tijd ?
    private Integer warranty;



    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "basket_id")
    private ShopBasket shopbasket;
    @Transient
    private Boolean isWireless ;



//constructors moeten leeg normaal gezien


    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufactory() {
        return manufactory;
    }

    public void setManufactory(String manufactory) {
        this.manufactory = manufactory;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getWarranty() {
        return warranty;
    }

    public void setWarranty(Integer warranty) {
        this.warranty = warranty;
    }

    public Double getPrice() {
        return price;
    }

    public ShopBasket getShopBasket() {
        return shopbasket;
    }

    public void setShopBasket(ShopBasket shopBasket) {
        this.shopbasket = shopbasket;
    }


    public Boolean getWireless() {
        return isWireless;
    }

    public void setWireless(Boolean wireless) {
        isWireless = wireless;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", manufactory='" + manufactory + '\'' +
                ", releaseDate=" + releaseDate +
                ", warranty=" + warranty +
                ", shopbasket=" + shopbasket +
                '}';
    }
}
