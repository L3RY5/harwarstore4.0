package com.realdolmen.teamPHP.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "hdd")
@NamedQueries({
        @NamedQuery( name = HDD.FILTER_RPM_ABOVE_NUMBER,query = "SELECT h FROM HDD h WHERE h.rpm > :rpm"),
        @NamedQuery( name = HDD.FILTER_PRICE_ABOVE_NUMBER,query = "SELECT h FROM HDD h WHERE h.price > :price")
}
)
public class HDD extends Harddrive {
    public static final String FILTER_RPM_ABOVE_NUMBER = "findHDDrpm";
    public static final String FILTER_PRICE_ABOVE_NUMBER = "filterOnPrice";


    private Long rpm;

    public HDD() {
    }

    public Long getRpm() {
        return rpm;
    }

    public void setRpm(Long rpm) {
        this.rpm = rpm;
    }


}
