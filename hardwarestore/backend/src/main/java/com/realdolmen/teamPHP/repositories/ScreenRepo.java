package com.realdolmen.teamPHP.repositories;


import com.realdolmen.teamPHP.domain.Screen;

import javax.persistence.EntityManager;
import javax.ws.rs.Produces;
import java.util.List;


@Produces
   public class ScreenRepo extends AbstractRepoProduct<Screen, Long> {


        public ScreenRepo() {
            super(Screen.class);
        }

    public ScreenRepo(EntityManager em) {
        super(em, Screen.class);
    }
        
        
        
       public List<Screen> filterOnPrice(double i){
           return em.createNamedQuery(Screen.FILTER_PRICE_ABOVE_NUMBER_SCREEN,Screen.class)
                   .setParameter("priceScreen",i)
                   .getResultList();
       }

    @Override
    public Screen findById(Long id) {
        return super.findById(id);
    }
}

