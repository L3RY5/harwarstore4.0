package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Login;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Produces;

@Produces
public class LoginRepo extends AbstractRepoProduct<Login, Long> {
    public LoginRepo() {
        super(Login.class);
    }

    protected LoginRepo(EntityManager em) {
        super(em, Login.class);
    }
    
    

    public List<Login> findIsAdmin(Boolean i) {
        return em.createNamedQuery(Login.FILTER_ISADMIN, Login.class)
                .setParameter("isAdmin", i)
                .getResultList();
    }

    public List<Login> findUserName(String i) {
        return em.createNamedQuery(Login.FIND_USERNAME, Login.class)
                .setParameter("username", i)
                .getResultList();
    }


}
