package com.realdolmen.teamPHP.domain;

import javax.persistence.*;

@Entity
@Table(name = "login")
@NamedQueries({
        @NamedQuery( name = Login.FILTER_ISADMIN,query = "SELECT h FROM Login h WHERE h.isAdmin = :isAdmin"),
        @NamedQuery( name = Login.FIND_USERNAME,query = "SELECT h FROM Login h WHERE h.username LIKE :username")
})
public class Login {
    public static final String FILTER_ISADMIN = "filteradmin";
    public static final String FIND_USERNAME = "findusername";


    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private String password;
    private Boolean isAdmin;
    public Login() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public String toString() {
        return "Login{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
