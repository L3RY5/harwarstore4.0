package com.realdolmen.teamPHP.mapper;

import com.realdolmen.teamPHP.domain.Login;
import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.domain.UserAccount;
import com.realdolmen.teamPHP.dtos.ShopbasketDTO;
import com.realdolmen.teamPHP.dtos.UserDTO;
import java.util.function.Function;

public class UserMapper  implements Function<UserAccount, UserDTO>{

    @Override
    public UserDTO apply(UserAccount useraccount) {
        UserDTO uto = new UserDTO(useraccount.getId(),useraccount.getName());
        /*if(useraccount.getShopbasket()!=null){
           ShopBasket shopBasket = useraccount.getShopbasket();
          uto.setShopbasket(new ShopbasketDTO(shopBasket.getId()));
        }*/
        return uto;
    }

}
