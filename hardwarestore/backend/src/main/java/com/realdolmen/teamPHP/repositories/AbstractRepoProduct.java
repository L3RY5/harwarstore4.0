package com.realdolmen.teamPHP.repositories;


import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

public class AbstractRepoProduct<C, T> {


    @PersistenceContext
    protected EntityManager em;

    private EntityTransaction transaction;
    private Class<C> entityClass;


    public AbstractRepoProduct(Class<C> entityClass) {
        this.entityClass = entityClass;
    }

    public AbstractRepoProduct(EntityManager em, Class<C> entityClass) {
        this.em = em;
        this.entityClass = entityClass;
    }

    public C findById(T id) {

        return em.find(entityClass, id);
    }



    @Transactional
    public void save(C c) {
        if (c != null) {
            begin();
            em.persist(c);
            commit();
        }
    }
    //@Transactional(value = Transactional.TxType.REQUIRED)

    @Transactional
    public void delete(T id) {
        begin();
        em.remove(em.find(entityClass, id));
        commit();
    }

    public List<C> findAll() {
        String className = entityClass.getName();
        return em.createQuery("select c from " + className + " c").getResultList();
    }

    @Transactional
    public void update(C c) {
        if (c != null) {
            begin();
            em.merge(c);
            commit();
        }
    }

    protected void commit() {
        if (transaction != null && transaction.isActive()) {
            transaction.commit();
        }
    }

    public void begin() {
        transaction = em.getTransaction();
        if (!transaction.isActive()) {
            transaction.begin();
        }
    }

    public void close() {
        if (em != null) {
            em.close();
        }
    }
}
