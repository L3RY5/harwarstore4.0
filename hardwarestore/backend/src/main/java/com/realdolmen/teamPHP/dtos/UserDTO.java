package com.realdolmen.teamPHP.dtos;

import com.realdolmen.teamPHP.domain.Login;
import com.realdolmen.teamPHP.domain.ShopBasket;

import javax.persistence.*;

public class UserDTO {


    private Long id;
    private String name;
    private Login login;
    private ShopBasket shopbasket;

    public UserDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public UserDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Login getLogin() {
        return login;
    }

    public UserDTO setLogin(Login login) {
        this.login = login;
        return this;
    }

    public ShopBasket getShopbasket() {
        return shopbasket;
    }

    public UserDTO setShopbasket(ShopBasket shopbasket) {
        this.shopbasket = shopbasket;
        return this;
    }
}
