package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Keyboard;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Produces;

@Produces
public class KeyboardRepo extends AbstractRepoProduct<Keyboard,Long> {

    public KeyboardRepo() {
        super(Keyboard.class);
    }

    protected KeyboardRepo(EntityManager em){
        super(em,Keyboard.class);
    }

    @Override
    public List<Keyboard> findAll() {
        return super.findAll();
    }

    @Override
    public Keyboard findById(Long id) {
        return super.findById(id);
    }



    public List<Keyboard> filterOnWirless(Boolean i) {
        return em.createNamedQuery(Keyboard.FILTER_WIRELESS_KEYBOARD, Keyboard.class)
                .setParameter("isWireless", i)
                .getResultList();
    }

    public List<Keyboard> filterPriceBtween(double priceMin, double priceMax) {
        return em.createNamedQuery(Keyboard.FILTER_PRICE_BETWEEN, Keyboard.class)
                .setParameter("priceMin", priceMin).setParameter("priceMax", priceMax)
                .getResultList();
    }

    public List<Keyboard> filterManufactuary(String manuFacts) {
        return em.createNamedQuery(Keyboard.FILTER_MANUFACTORY_KEYBOARD, Keyboard.class)
                .setParameter("manufactory", manuFacts)
                .getResultList();
    }

    public List<Keyboard> filterPriceAndWireless(double priceMin, double priceMax,Boolean wireless) {
        return em.createNamedQuery(Keyboard.FILTER_PRICE_AND_WIRELESS, Keyboard.class)
                .setParameter("priceMinWireless", priceMin).setParameter("priceMaxWireless", priceMax).setParameter("idwireless",wireless)
                .getResultList();
    }

    public List<Keyboard> filterNameLike(String NameLike) {
        return em.createNamedQuery(Keyboard.FILTER_NAME_LIKE, Keyboard.class)
                .setParameter("manufactoryOption", NameLike)
                .getResultList();
    }



}
