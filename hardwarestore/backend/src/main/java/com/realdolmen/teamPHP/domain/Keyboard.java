package com.realdolmen.teamPHP.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "keyboard")
@NamedQueries({
        @NamedQuery( name = Keyboard.FILTER_WIRELESS_KEYBOARD,query = "SELECT h FROM Keyboard h WHERE h.isWireless = :isWireless"),
        @NamedQuery( name = Keyboard.FILTER_MANUFACTORY_KEYBOARD,query = "SELECT h FROM Keyboard h WHERE h.manufactory = :manufactory"),
        @NamedQuery( name = Keyboard.FILTER_PRICE_BETWEEN,query = "SELECT h FROM Keyboard h WHERE h.price between :priceMin and :priceMax"),
        @NamedQuery( name = Keyboard.FILTER_PRICE_AND_WIRELESS,query = "SELECT h FROM Keyboard h WHERE (h.price between :priceMinWireless and :priceMaxWireless ) and (h.isWireless = :idwireless )"),
        @NamedQuery( name = Keyboard.FILTER_NAME_LIKE,query = "SELECT h FROM Keyboard h WHERE h.name LIKE CONCAT('%',:manufactoryOption,'%')"),
}
)
public class Keyboard extends Product  {
    public static final String FILTER_WIRELESS_KEYBOARD = "findWirelessKeybor";
    public static final String FILTER_MANUFACTORY_KEYBOARD = "filterOnManufactoryKeyb";
    public static final String FILTER_PRICE_BETWEEN = "filterPriceBetween";
    public static final String FILTER_PRICE_AND_WIRELESS = "filterPriceAndWireless";
    public static final String FILTER_NAME_LIKE = "filterNameLikeKeyboard";
    private Boolean isWireless = false;
    public Keyboard() {
    }

    public Boolean getWireless() {
        return isWireless;
    }

    public void setWireless(Boolean wireless) {
        isWireless = wireless;
    }


}
