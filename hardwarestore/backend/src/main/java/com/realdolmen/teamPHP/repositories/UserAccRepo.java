package com.realdolmen.teamPHP.repositories;

        import com.realdolmen.teamPHP.domain.UserAccount;

        import javax.annotation.PostConstruct;
        import javax.persistence.EntityManager;
        import javax.persistence.PersistenceContext;
        import javax.ws.rs.Produces;
        import java.io.Serializable;
        import java.util.List;
@Produces
public class UserAccRepo extends AbstractRepoProduct<UserAccount,Long> implements Serializable {

    @PersistenceContext
    private EntityManager entityManager;

    public UserAccRepo() {
        super(UserAccount.class);
    }

    @PostConstruct
    public void init(){

    }

    public UserAccRepo(EntityManager em) {
        super(em, UserAccount.class);
        this.entityManager = em;
    }


  /*  @Override
    public List<UserAccount> findUserByName(String Searchname) {
        return super.findUserByName(Searchname);
    }*/
    public UserAccount filterUserName(String nameLike) {
        return em.createNamedQuery(UserAccount.FILTER_USERACCOUNT_LIKE, UserAccount.class)
                .setParameter("userAcount", nameLike)
                .getSingleResult();
    }




}