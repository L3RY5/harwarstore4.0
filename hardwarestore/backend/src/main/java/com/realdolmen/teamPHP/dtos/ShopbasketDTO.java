package com.realdolmen.teamPHP.dtos;
import com.realdolmen.teamPHP.domain.ShopBasket;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

public class ShopbasketDTO {
    private Long id;


    private List<ProductDTO> productDTO;

    public ShopbasketDTO(Long id) {
        this.id = id;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ProductDTO> getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(List<ProductDTO> productDTO) {
        this.productDTO = productDTO;
    }
}
