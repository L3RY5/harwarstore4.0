package com.realdolmen.teamPHP.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "mouse")
@NamedQueries({
        @NamedQuery( name = Mouse.FILTER_WIRELESS_MOUSE,query = "SELECT h FROM Mouse h WHERE h.isWireless = :isWireless"),
        @NamedQuery( name = Mouse.FILTER_MANUFACTORY_MOUSE,query = "SELECT h FROM Mouse h WHERE h.manufactory = :manufactory"),
        @NamedQuery( name = Mouse.FILTER_PRICE_BETWEEN_MOUSE,query = "SELECT h FROM Mouse h WHERE h.price between :priceMin and :priceMax"),
        @NamedQuery( name = Mouse.FILTER_PRICE_AND_WIRELESS_MOUSE,query = "SELECT h FROM Mouse h WHERE (h.price between :priceMinWireless and :priceMaxWireless ) and (h.isWireless = :idwireless )"),
        @NamedQuery( name = Mouse.FILTER_NAME_LIKE,query = "SELECT h FROM Mouse h WHERE h.name LIKE CONCAT('%', :manufactory, '%') "),
}
)
public class Mouse extends Product {
    public static final String FILTER_WIRELESS_MOUSE = "findWirelessMouse";
    public static final String FILTER_MANUFACTORY_MOUSE = "filterOnManufactoryMouse";
    public static final String FILTER_PRICE_BETWEEN_MOUSE = "filterPriceBetweenMouse";
    public static final String FILTER_PRICE_AND_WIRELESS_MOUSE = "filterPriceAndWirelessMouse";
    public static final String FILTER_NAME_LIKE = "filterNameLikeMouse";

    private Boolean isWireless = false;
    public Mouse() {
    }

    public Boolean getWireless() {
        return isWireless;
    }

    public void setWireless(Boolean wireless) {
        isWireless = wireless;
    }

}
