package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.ShopBasket;
import javax.persistence.EntityManager;
import javax.ws.rs.Produces;


@Produces
public class ShopbasketRepo extends AbstractRepoProduct<ShopBasket,Long> {
    public ShopbasketRepo() {
        super(ShopBasket.class);
    }

    public ShopbasketRepo(EntityManager em) {
        super(em, ShopBasket.class);
    }
    
    
}
