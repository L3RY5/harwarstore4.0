package com.realdolmen.teamPHP.services;

import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.domain.Mouse;
import com.realdolmen.teamPHP.repositories.MouseRepo;

import javax.inject.Inject;
import java.util.List;

public class MouseService {
    @Inject
    private MouseRepo mouseRepo;

    public MouseService() {
    }

    public List<Mouse> findAllMouseService(){return  mouseRepo.findAll();}
    public Mouse findMouseOnId(Long id){
        return mouseRepo.findById(id);
    }

    public  List<Mouse>filterOnWirelessMouseService(Boolean i){return mouseRepo.filterOnWirless(i);}

    public List<Mouse>filterPriceBtweenMouseService(double priceMin, double priceMax){
        return mouseRepo.filterPriceBtween(priceMin,priceMax);
    }
    public List<Mouse> filterManufactuaryMouseService(String manuFacts) {
        return mouseRepo.filterManufactuary(manuFacts);
    }

    public List<Mouse> filterPriceAndWirelessMouseService(double priceMin, double priceMax,Boolean wireless) {
        return mouseRepo.filterPriceAndWireless(priceMin,priceMax,wireless);
    }
    public List<Mouse> filterNameLikeMouseService(String NameLike) {
        return mouseRepo.filterNameLike(NameLike);
    }
}
