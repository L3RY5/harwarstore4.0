package com.realdolmen.teamPHP.services;

import com.realdolmen.teamPHP.domain.SSD;
import com.realdolmen.teamPHP.repositories.SsdRepo;

import javax.inject.Inject;
import java.util.List;

public class SsdService {
    @Inject
    private SsdRepo ssdRepo;

    public SsdService() {
    }

    public List<SSD> findAllSsdService(){return  ssdRepo.findAll();}


    public List<SSD>filterPriceAboveSsdService(double priceMin){
        return ssdRepo.filterOnPrice(priceMin);
    }
  public SSD findById(Long id){return ssdRepo.findById(id);}

}
