package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.HDD;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Produces;

@Produces
public class HddRepo extends AbstractRepoProduct<HDD,Long> {
   
    public HddRepo() {
        super(HDD.class);
    }

    protected HddRepo(EntityManager em) {
        super(em,HDD.class);
    }

    public List<HDD> filterOnRpm(Long i) {
        return em.createNamedQuery(HDD.FILTER_RPM_ABOVE_NUMBER, HDD.class)
                .setParameter("rpm", i)
                .getResultList();
    }
        public List<HDD> filterOnPrice ( double i){
            return em.createNamedQuery(HDD.FILTER_PRICE_ABOVE_NUMBER, HDD.class)
                    .setParameter("price", i)
                    .getResultList();
        }

}
