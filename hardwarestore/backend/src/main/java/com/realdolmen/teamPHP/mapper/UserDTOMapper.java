package com.realdolmen.teamPHP.mapper;

import com.realdolmen.teamPHP.domain.UserAccount;
import com.realdolmen.teamPHP.dtos.UserDTO;

import java.util.function.Function;

public class UserDTOMapper implements Function<UserDTO, UserAccount> {

    //om een useracout te maken gebruike dto
    @Override
    public UserAccount apply(UserDTO u) {

        UserAccount ua = new UserAccount();
        ua.setId(u.getId());
        ua.setName(u.getName());


        return ua;

    }
}
