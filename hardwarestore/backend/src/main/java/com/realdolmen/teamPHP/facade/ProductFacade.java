package com.realdolmen.teamPHP.facade;

import com.realdolmen.teamPHP.builders.ProductDTObuilder;

import com.realdolmen.teamPHP.domain.Mouse;
import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.domain.*;

import com.realdolmen.teamPHP.dtos.ProductDTO;
import com.realdolmen.teamPHP.services.*;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;


public class ProductFacade implements Serializable {

    @Inject
    private KeyboardService keyboardService;
    @Inject
    private MouseService mouseService;
    @Inject
    private ScreenService screenService;
    @Inject
    private SsdService ssdService;
    @Inject
    private HddService hddService;

    private List<ProductDTO> productDTOS = null;

    public ProductFacade() {
    }

    public List<ProductDTO> findAll(String product) {
        productDTOS = new ArrayList<>();
        switch (product.toLowerCase()) {
            case "keyboard":
                for (Keyboard k : keyboardService.findAllKeyBService()) {
                    productDTOS.add(new ProductDTObuilder().setId(k.getId()).setPrice(k.getPrice()).setName(k.getName()).setManufactory(k.getManufactory()).setReleaseDate(k.getReleaseDate())
                            .setWarranty(k.getWarranty()).setWireless(k.getWireless()).build());
                }
                break;
            case "mouse":
                for (Mouse m : mouseService.findAllMouseService()) {
                    productDTOS.add(new ProductDTObuilder().setId(m.getId()).setPrice(m.getPrice()).setName(m.getName()).setManufactory(m.getManufactory()).setReleaseDate(m.getReleaseDate())
                            .setWarranty(m.getWarranty()).setWireless(m.getWireless()).build());
                }
                break;

            case "screen":
                for (Screen s : screenService.findAllScreenService()) {
                    productDTOS.add(new ProductDTObuilder().setId(s.getId()).setPrice(s.getPrice()).setName(s.getName()).setManufactory(s.getManufactory()).setReleaseDate(s.getReleaseDate())
                            .setWarranty(s.getWarranty()).build());
                }
                break;
            case "ssd":
                for (SSD sd : ssdService.findAllSsdService()) {
                    productDTOS.add(new ProductDTObuilder().setId(sd.getId()).setPrice(sd.getPrice()).setName(sd.getName()).setManufactory(sd.getManufactory()).setReleaseDate(sd.getReleaseDate())
                            .setWarranty(sd.getWarranty()).build());
                }
                break;
            case "hdd":
                for (HDD hd : hddService.findAllHddService()) {
                    productDTOS.add(new ProductDTObuilder().setId(hd.getId()).setPrice(hd.getPrice()).setName(hd.getName()).setManufactory(hd.getManufactory()).setReleaseDate(hd.getReleaseDate())
                            .setWarranty(hd.getWarranty()).setRpm(hd.getRpm()).build());
                }
                break;
            default:
                System.out.println(product + "werkt nog niet");
        }
        return productDTOS;


    }

    public ProductDTO findProduct(String product, Long id) {
        productDTOS = new ArrayList<>();
        switch (product.toLowerCase()) {
            case "keyboard":
                Keyboard k = keyboardService.findKeyboardOnId(id);
                productDTOS.add(new ProductDTObuilder().setId(k.getId()).setPrice(k.getPrice()).setName(k.getName()).setManufactory(k.getManufactory()).setReleaseDate(k.getReleaseDate())
                        .setWarranty(k.getWarranty()).setWireless(k.getWireless()).build());
                break;
            case "mouse":
                Mouse m = mouseService.findMouseOnId(id);
                productDTOS.add(new ProductDTObuilder().setId(m.getId()).setPrice(m.getPrice()).setName(m.getName()).setManufactory(m.getManufactory()).setReleaseDate(m.getReleaseDate())
                        .setWarranty(m.getWarranty()).setWireless(m.getWireless()).build());
                break;
            case "screen":
                Screen s = screenService.findBuId(id);
                productDTOS.add(new ProductDTObuilder().setId(s.getId()).setPrice(s.getPrice()).setName(s.getName()).setManufactory(s.getManufactory()).setReleaseDate(s.getReleaseDate())
                        .setWarranty(s.getWarranty()).build());
                break;
            case "hdd":
                HDD hd = hddService.findById(id);
                productDTOS.add(new ProductDTObuilder().setId(hd.getId()).setPrice(hd.getPrice()).setName(hd.getName()).setManufactory(hd.getManufactory()).setReleaseDate(hd.getReleaseDate())
                        .setWarranty(hd.getWarranty()).build());
                break;
            case "ssd":
                SSD ssd = ssdService.findById(id);
                productDTOS.add(new ProductDTObuilder().setId(ssd.getId()).setPrice(ssd.getPrice()).setName(ssd.getName()).setManufactory(ssd.getManufactory()).setReleaseDate(ssd.getReleaseDate())
                        .setWarranty(ssd.getWarranty()).build());
                break;
            default:
                System.out.println(product + "werkt nog niet");

        }

        return productDTOS.get(0);
    }


}
