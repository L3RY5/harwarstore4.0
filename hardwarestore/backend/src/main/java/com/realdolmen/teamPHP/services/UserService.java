package com.realdolmen.teamPHP.services;

import com.realdolmen.teamPHP.domain.UserAccount;
import com.realdolmen.teamPHP.repositories.UserAccRepo;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

public class UserService {

@Inject
private UserAccRepo userAccRepo;

    public UserService() {
    }

    @PostConstruct
    public void init(){
        System.out.println("UserService constructed");
    }

    public UserAccount findUser(String uname){return userAccRepo.filterUserName(uname);}

    public void addUser(UserAccount userAccount) {
        userAccRepo.save(userAccount);
    }
}
