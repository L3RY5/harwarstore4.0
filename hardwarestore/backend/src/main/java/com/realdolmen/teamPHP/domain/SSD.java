package com.realdolmen.teamPHP.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "ssd")
@NamedQueries({
        @NamedQuery( name = SSD.FILTER_PRICE_ABOVE_NUMBER_SSD,query = "SELECT s FROM SSD s WHERE s.price > :priceSsd")

}
)
public class SSD extends Harddrive {
    public static final String FILTER_PRICE_ABOVE_NUMBER_SSD = "filterOnPriceSsd";



    public SSD() {
    }


}
