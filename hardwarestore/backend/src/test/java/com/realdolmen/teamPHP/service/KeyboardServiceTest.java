package com.realdolmen.teamPHP.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;
import com.realdolmen.teamPHP.services.KeyboardService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class KeyboardServiceTest {

    @Mock
    private KeyboardRepo keyboardrepo;
    private Keyboard KeyboardTest;

    @InjectMocks
    private KeyboardService keyboardService;

    private List<Keyboard> keyboards;

    @Before
    public void init() {
        keyboards = new ArrayList<>();
    }

    @Test
    public void findAllKeyBServiceTest() {
        when(keyboardrepo.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Keyboard())));

        List<Keyboard> result = keyboardService.findAllKeyBService();

        assertEquals(result.size(), 1);

        verify(keyboardrepo, times(1)).findAll();
        verifyNoMoreInteractions(keyboardrepo);

    }


    @Test
    public void filterOnWirelessKeyBTest() {
        when(keyboardrepo.filterOnWirless(true)).thenReturn(new ArrayList<>(Arrays.asList(new Keyboard())));
        List<Keyboard> kResults = keyboardService.filterOnWirelessService(true);
        assertEquals(kResults.size(), 1);
        verify(keyboardrepo, times(1)).filterOnWirless(true);


    }

    @Test
    public void filterOnNameLikeBTest() {
        when(keyboardrepo.filterNameLike("%p%")).thenReturn(new ArrayList<>(Arrays.asList(new Keyboard())));

        List<Keyboard> kResults = keyboardService.filterNameLikeService("%p%");
        assertTrue(String.valueOf(kResults.equals("%p%")), true);
    }


    @Ignore
    @Test
    public void filterPriceBetweenKeyBTest() {
        //given
        List<Keyboard> kBoards = new ArrayList<>();
        when(keyboardrepo.filterPriceBtween(10.00, 20.00)).thenReturn(kBoards);
        //then
        List<Keyboard> kResults = keyboardService.filterPriceBtweenService(10.00, 20.00);
        //verify
        assertEquals(kBoards, kResults);
        assertTrue(kResults.get(5).getPrice() > 10.00 && kResults.get(0).getPrice() < 20.00);
        verify(keyboardrepo, times(1)).filterPriceBtween(10.00, 20.00);


    }


}

