package com.realdolmen.teamPHP.domain;

import com.realdolmen.teamPHP.domain.Login;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoginPersistence extends AbstractPersistence {
    @Test
    public void PersistLogin(){
        Login login = new Login();
       // login.setId(7000L);
        login.setUsername("Eerste Admin");
        login.setIsAdmin(true);
        login.setPassword("passwordAdmin");
        em.persist(login);
        assertNotNull(login.getId());
        assertEquals(login.getUsername(),"Eerste Admin");

    }

    @Test
    public void Load(){
        Login login=(em.find(Login.class,6000L));
        assertNotNull(login);
        assertEquals("Sweethearts",login.getUsername());
    }


    


}
