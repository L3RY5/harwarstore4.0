package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.HDD;
import com.realdolmen.teamPHP.domain.Login;
import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.repositories.HddRepo;
import com.realdolmen.teamPHP.repositories.LoginRepo;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class LoginRepoTest {

    private LoginRepo loginrepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;


    @BeforeClass
    public static void initCLass()
    {
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        loginrepo = new LoginRepo(em);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
    }

    @Test
    public void findLoginByIdTest() {
        Login login = loginrepo.findById(2000L);
        assertNotNull(login);
        assertEquals(login.getUsername(),"Pink Kisses");
    }

    @Test
    public void addLoginTest() throws ParseException {

        Login login = new Login();
        login.setUsername("Pink Kisses");
       login.setIsAdmin(true);
       login.setPassword("fjfksoezs");

        loginrepo.save(login);

        System.out.println("adding "+login.getClass()+ "To Database");

        Login login1=em.find(Login.class,login.getId());
        //assertEquals(5555,hdd1);
        assertEquals("Pink Kisses",login1.getUsername());
        assertEquals(true,login1.getIsAdmin());
        assertEquals("fjfksoezs",login1.getPassword());


    }

    @Test
    public  void deleteLoginTest(){
        loginrepo.delete(2000L);
        assertEquals(null,em.find(Login.class,2000L));
    }

    @Test
    public void updateLoginTest(){
        Login login = em.find(Login.class,1000l);
        loginrepo.begin();
        em.detach(login);
        login.setIsAdmin(false);
        em.flush();
        em.clear();
        loginrepo.update(login);
        assertEquals(false,em.find(Login.class,1000L).getIsAdmin());
        //assertNotNull(em.find(HDD.class,1000L));
    }

    @Test
    public void findLoginUsernameTest(){
        List<Login> logins = loginrepo.findUserName("P%");
        for (Login login:logins){
            System.out.println(login.toString());
            assertTrue(login.getUsername().contains("P"));
        }

    }
  @Test
    public void filterLoginOnAdminTest(){
        List<Login> logins = loginrepo.findIsAdmin(true);
        for (Login login:logins){
            System.out.println(login.toString());
            assertEquals(true,  login.getIsAdmin());
        }

    }


    @After
    public void exit() {
        loginrepo.close();
    }

    @AfterClass
    public static void exitClass(){
        emf.close();
    }

}
