package com.realdolmen.teamPHP.repositories;

import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.domain.ShopBasket;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;
import com.realdolmen.teamPHP.repositories.ShopbasketRepo;
import org.junit.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
public class KeyboardRepoTest {
    private KeyboardRepo keyboardRepo;
    private ShopbasketRepo shopbasketRepo;
    private static EntityManagerFactory emf;
    private EntityManager em;
    private DateTimeFormatter formatter;

    @BeforeClass
    public static void initCLass()
    {
        emf = Persistence.createEntityManagerFactory("HardwareStorePersistenceUnit");
    }

    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        keyboardRepo = new KeyboardRepo(em);
       // formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD");
        shopbasketRepo = new ShopbasketRepo(em);

    }

    @Test
    public void findKeyboardall() {

        List<Keyboard> keyboards  = keyboardRepo.findAll();
        keyboards.stream().forEach(kb -> System.out.println(kb.toString()));
        assertTrue(keyboards.size() >0);
    }

    @Test
    public void findKeyboardByIdTest() {
        Keyboard keyboard = keyboardRepo.findById(8000L);
        assertNotNull(keyboard);
        assertEquals(keyboard.getName(),"Harddrive Noob");
    }
    @Test
    public void addKeyboardTest() throws ParseException {
        ShopBasket sb = new ShopBasket();


        sb.setId(5577L);



     //   shopbasketRepo.save(sb);

        ShopBasket sbBefor=em.find(ShopBasket.class,sb.getId());

        Keyboard keyboard = new Keyboard();
        keyboard.setManufactory("manufactory33");
        keyboard.setName("Keyboard Ultra33");
        keyboard.setPrice(11.50);
        LocalDate date1= LocalDate.of(2014, Month.APRIL,1);
        keyboard.setReleaseDate(date1);
        System.out.println(keyboard.getReleaseDate());
        keyboard.setWarranty(2);

        keyboard.setShopBasket(sbBefor);
        keyboard.setWireless(true);
        keyboardRepo.save(keyboard);

        System.out.println("adding "+keyboard.getClass()+ "To Database");

        Keyboard keyboard1=em.find(Keyboard.class,keyboard.getId());
        assertEquals(keyboard,keyboard1);
        assertEquals("Keyboard Ultra33",keyboard1.getName());
        assertEquals("manufactory33",keyboard1.getManufactory());
        assertEquals(date1,keyboard1.getReleaseDate());
        assertEquals(2,(int)keyboard1.getWarranty());



    }

    @Test
    public  void deleteKeyboardTest(){
        keyboardRepo.delete(22000L);
        assertEquals(null,em.find(Keyboard.class,22000L));
    }
    @Test
    public void updateKeyboardTest(){
        Keyboard keyboard = em.find(Keyboard.class,6000l);
        keyboardRepo.begin();
        em.detach(keyboard);
        keyboard.setManufactory("manufactory22");
        em.flush();
        em.clear();
        keyboardRepo.update(keyboard);
        assertEquals("manufactory22",em.find(Keyboard.class,6000L).getManufactory());
        //assertNotNull(em.find(HDD.class,1000L));
    }

    @Test
    public void filterIswirelessTest(){
        List<Keyboard> keyboards = keyboardRepo.filterOnWirless(false);
       /* for (Keyboard kb:keyboards){
            System.out.println(kb.toString());
        }*/
       keyboards.stream().forEach(kb -> System.out.println(kb.toString()));
        assertTrue(keyboards.size() >0);
    }

    @Test
    public void filterOnMinMaxPrice(){
        List<Keyboard> keyboards = keyboardRepo.filterPriceBtween(10,20.00);
        /*for (Keyboard kb:keyboards){
            System.out.println(kb.toString());
        }*/
        keyboards.stream().forEach(kb -> System.out.println(kb.toString()));
        assertTrue(keyboards.size() >0);
    }

    @Test
    public void filterManufatoryTest(){
        List<Keyboard> keyboards = keyboardRepo.filterManufactuary("manufactory3");
        /*for (Keyboard kb:keyboards){
            System.out.println(kb.toString());
        }*/
        keyboards.stream().forEach(kb -> System.out.println(kb.toString()));
        assertTrue(keyboards.size() >0);
    }

    @Test
    public void filterPriceAndWirelessTess(){
        List<Keyboard> keyboards = keyboardRepo.filterPriceAndWireless(10.00,20.00,true);
        keyboards.stream().forEach(kb -> System.out.println(kb.toString()));
        assertTrue(keyboards.size() >0);
    }

    @Test
    public void filterNameLikeTest(){
        List<Keyboard> keyboards = keyboardRepo.filterNameLike("Hard");

        keyboards.stream().forEach(kb -> System.out.println(kb.toString()));
        assertTrue(keyboards.size() >0);
    }
    @After
    public void exit() {
        keyboardRepo.close();
    }

    @AfterClass
    public static void exitClass(){
        emf.close();
    }

}
