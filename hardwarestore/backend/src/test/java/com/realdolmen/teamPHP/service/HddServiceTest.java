package com.realdolmen.teamPHP.service;

import com.realdolmen.teamPHP.domain.HDD;
import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.repositories.HddRepo;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;
import com.realdolmen.teamPHP.services.HddService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@RunWith(MockitoJUnitRunner.class)
public class HddServiceTest {
    @Mock
    private HddRepo hddrepo;
    private HDD hddTest;

    @InjectMocks
    private HddService hddService;

    private List<HDD> hdds;

    @Before
    public void init() {
        hdds = new ArrayList<>();
    }

    @Test
    public void findAllHddTest() {
        when(hddrepo.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new HDD())));

        List<HDD> result = hddService.findAllHddService();

        assertEquals(result.size(), 1);

        verify(hddrepo, times(1)).findAll();
        verifyNoMoreInteractions(hddrepo);

    }
}
