package com.realdolmen.teamPHP.service;


import com.realdolmen.teamPHP.domain.Keyboard;
import com.realdolmen.teamPHP.domain.SSD;
import com.realdolmen.teamPHP.repositories.KeyboardRepo;
import com.realdolmen.teamPHP.repositories.SsdRepo;
import com.realdolmen.teamPHP.services.SsdService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SsdServiceTest {

    @Mock
    private SsdRepo ssdrepo;
    private SSD SddTest;

    @InjectMocks
    private SsdService ssdService;

    private List<SSD> ssds;

    @Before
    public void init() {
        ssds = new ArrayList<>();
    }

    @Test
    public void findAllSsdServiceTest() {
        when(ssdrepo.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new SSD())));

        List<SSD> result = ssdService.findAllSsdService();

        assertEquals(result.size(), 1);

        verify(ssdrepo, times(1)).findAll();
        verifyNoMoreInteractions(ssdrepo);

    }

}
